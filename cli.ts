#!/usr/bin/env node
import glob from "glob";
import "ts-node/register";
import util from "util";
import fs from "fs";

import * as R from "rxjs";
import * as op from "rxjs/operators";

import path from "path";

glob("**/*.env.json.ts",{
    ignore:"**/node_modules/**"
},(error,files)=>{

    R.from(files).pipe(op.concatMap(async file=>{
        const filepath = path.join(process.cwd(),file);
        const content = await import(filepath)
        const output = filepath.replace(path.extname(filepath),"");
        return {
            filepath,
            content:content.default,
            output
        }
    }),op.concatMap(async (info)=>{
        const write = util.promisify(fs.writeFile);
        write(info.output,JSON.stringify(info.content))
        return info
    }),op.tap(info=>{
        console.log(info.output);
    })).toPromise()
})